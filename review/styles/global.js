import {StyleSheet} from "react-native";

export const globalStyles = StyleSheet.create({
    container: {
        flex: 1
    },
    titleText: {
        fontFamily: "nunito-bold",
        fontSize: 24,
        color: "#333"
    }
})