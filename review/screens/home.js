import {Text, View, StyleSheet, Button, FlatList, TouchableOpacity} from "react-native";
import {globalStyles} from "../styles/global.js";
import {useState} from "react";
import Card from "./card.js";

export default function Home({navigation}){

    const [reviews, setReviews] = useState([
        {title: "Burgerei", rating: 5, body:"Super Burgers"},
        {title: "Lavinya", rating: 4, body:"Gute Kebabs"},
        {title: "Salz und Pfeffer", rating: 3, body:"Alles quer durch"},
    ]);

    return (
        <View>
            <FlatList data={reviews} renderItem={ ({item})=> (
                <TouchableOpacity onPress={()=> navigation.navigate("Details",item)}>
                    <Card>
                        <Text style={globalStyles.titleText}>{item.title}</Text>
                    </Card>
                </TouchableOpacity>
            )}/>
        </View>
    )
}
