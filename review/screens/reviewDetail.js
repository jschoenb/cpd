import {Text, View, StyleSheet, Button} from "react-native";
import {globalStyles} from "../styles/global.js";
import Card from "./card.js";

export default function ReviewDetail({navigation, route}) {

    const {title,body,rating} = route.params;

    return (
        <Card>
            <Text>{title}</Text>
            <Text>{body}</Text>
            <Text>{rating}</Text>
        </Card>
    )
}