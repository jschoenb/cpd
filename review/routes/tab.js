import {NavigationContainer} from "@react-navigation/native";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import HomeStackNavigation from "./homeStack.js";
import AboutStackNavigation from "./aboutStack.js";
import {Ionicons} from "@expo/vector-icons";

const Tab = createBottomTabNavigator();

export default function RootTabNavigator() {
    return (
        <NavigationContainer>
            <Tab.Navigator initialRouteName="Home" screenOptions={
                ({route})=>({
                    tabBarIcon : ({focused, color, size}) => {
                        let iconName;
                        if(route.name ==="Home"){
                            iconName = focused ? 'home' : 'home-outline';
                        } else if(route.name ==="About"){
                            iconName = focused ? 'information-circle' : 'information-circle-outline';
                        }
                        return <Ionicons name={iconName} size={size} color={color}/>
                    },
                    tabBarActiveTintColor: 'tomato',
                    tabBarInactiveTintColor: 'gray'
                })
            }>
                <Tab.Screen name="Home" component={HomeStackNavigation} options={{headerShown:false}}/>
                <Tab.Screen name="About" component={AboutStackNavigation} options={{headerShown:false}} />
            </Tab.Navigator>
        </NavigationContainer>
    )
}