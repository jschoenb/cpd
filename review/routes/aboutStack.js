import {createNativeStackNavigator} from "@react-navigation/native-stack";
import About from "../screens/about.js";

const AboutStack = createNativeStackNavigator();

export default function AboutStackNavigation() {
    return (
            <AboutStack.Navigator initialRouteName="About" screenOptions={
                {
                    headerStyle: {
                        backgroundColor: "#f4ad1e"
                    },
                    headerTintColor: '#fff',
                    headerTitleStyle: {
                        fontWeight: 'bold'
                    }
                }
            }>
                <AboutStack.Screen name="About" component={About}/>
            </AboutStack.Navigator>
    )
}