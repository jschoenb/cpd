import {createNativeStackNavigator} from "@react-navigation/native-stack";
import {NavigationContainer} from "@react-navigation/native";
import Home from "../screens/home.js";
import ReviewDetail from "../screens/reviewDetail.js";

const HomeStack = createNativeStackNavigator();

export default function HomeStackNavigation() {
    return (
            <HomeStack.Navigator initialRouteName="Home" screenOptions={
                {
                    headerStyle: {
                        backgroundColor: "#f4ad1e"
                    },
                    headerTintColor: '#fff',
                    headerTitleStyle: {
                        fontWeight: 'bold'
                    }
                }
            }>
                <HomeStack.Screen name="Home" component={Home}/>
                <HomeStack.Screen name="Details" component={ReviewDetail}/>
            </HomeStack.Navigator>
    )
}