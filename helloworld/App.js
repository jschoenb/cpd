import { StatusBar } from 'expo-status-bar';
import {Button, FlatList, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native';
import {useState} from "react";

export default function App() {
  const [people, setPeople] = useState([
    {name:'Hannes', key:'1'},
    {name:'Franz', key:'2'},
    {name:'Susi', key:'3'},
    {name:'Maria', key:'4'},
    {name:'Karl', key:'5'},
    {name:'Susi', key:'6'},
    {name:'Maria', key:'7'},
    {name:'Karl', key:'8'},
  ]);

  const pressHandler = (key)=>{
    setPeople((prevState)=>{
      return prevState.filter(person => person.key != key)
    })
  }

  return (
    <View style={styles.container}>
      <FlatList numColumns={2}r data={people} renderItem={({item})=>(
          <TouchableOpacity onPress={()=>pressHandler(item.key)}>
            <Text style={styles.item}> {item.name} </Text>
          </TouchableOpacity>
      )
      }/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 40,
    paddingHorizontal: 20
  },
  item: {
    marginTop: 24,
    padding: 30,
    backgroundColor: 'pink',
    fontSize: 20
  }
});
