import { StatusBar } from 'expo-status-bar';
import {FlatList, Keyboard, StyleSheet, Text, TouchableWithoutFeedback, View} from 'react-native';
import {useState} from "react";
import Header from "./components/header.js";
import TodoItem from "./components/todoItem.js";
import AddTodo from "./components/addTodo.js";

export default function App() {
  const [todos, setTodos] = useState([
    { text: 'buy coffee', key: '1' },
    { text: 'create an app', key: '2' },
    { text: 'play on the switch', key: '3' }
  ]);

  const pressHandler=(key)=>{
    setTodos(prevTodos =>{
      return prevTodos.filter(todo => todo.key != key);
    })
  }

  const submitHandler =(text) => {
    setTodos(prevTodos => {
      return [
        {text, key: Math.random().toString()},
          ...prevTodos
      ];
    });
  }

  return (
      <TouchableWithoutFeedback onPress={()=>{Keyboard.dismiss()} }>
        <View style={styles.container}>
          {/*Header*/}
          <Header/>
          <View style={styles.content}>
            <AddTodo submitHandler={submitHandler}/>
            <View style={styles.list}>
              <FlatList
                  data={todos}
                  renderItem={({ item }) => (
                      <TodoItem item={item} pressHandler={pressHandler}/>
                  )}
              />
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  content: {
    padding: 40,
    flex: 1
  },
  list: {
    marginTop: 20,
    flex: 1
  },
});