import {Button, TextInput, View} from "react-native";
import {useState} from "react";

export default function AddTodo({submitHandler}){
    const [text, setText] = useState("");

    const changeHandler = (val)=> {
        setText(val);
    }

    return (
        <View>
            <TextInput style={styles.input} placeholder='new todo...' onChangeText={changeHandler}
                       value={text}></TextInput>
            <Button title="Add Todo" color='coral' onPress={()=>submitHandler(text)} />
        </View>
    )
}

const styles = {
    input: {
        marginBottom: 10,
        paddingHorizontal: 8,
        paddingVertical: 6,
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    }
}