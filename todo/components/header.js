import {View, Text} from "react-native";

export default function Header(){
    return (
        <View style={styles.header}>
            <Text style={styles.title}>Meine Todos</Text>
        </View>
    )
}

const styles = {
    header: {
        height: 80,
        paddingTop: 40,
        backgroundColor: 'coral'
    },
    title: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold'
    }
}