import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  final Widget titleSection = Container(
    padding: const EdgeInsets.all(32),
    child: Row(
      children: [
        //Text
        Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: const Text("Toller Campingplatz",
                      style: TextStyle(
                        fontWeight: FontWeight.bold
                      )
                  ),
                ),
                Text("Hagenberg, Österreich",
                  style: TextStyle(
                    color: Colors.grey[600]
                  ),
                )
              ],
            )
        ),
        //Icon
        Icon(Icons.star,color: Colors.red[500],),
        //Zahl
        const Text("35")
      ],
    ),
  );

  Column _buildButtomColumn(Color color, IconData icon, String label){
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon,color: color),
        Container(
          margin: const EdgeInsets.only(top:10),
          child: Text(label,
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.bold,
              color:color
            ),
          ),
        )
      ],
    );
  }

  Widget textSection = const Padding(
    padding: EdgeInsets.all(30),
     child: Text("Im ruhigen Hagenberg gibt es einen tollen Campingplatz!....."),
  );

  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;

    Widget buttonSection = Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        _buildButtomColumn(color, Icons.call, "CALL"),
        _buildButtomColumn(color, Icons.near_me, "ROUTE"),
        _buildButtomColumn(color, Icons.share, "SHARE"),
      ],
    );



    return MaterialApp(
      title: "Flutter Layout Example",
      theme: ThemeData(
        primarySwatch: Colors.blue
      ),
      home: Scaffold(
        appBar: AppBar(
            title: const Text("Layout Example"),
        ),
        body: Column(
          children: [
            Image.network("https://cf.bstatic.com/xdata/images/hotel/max1024x768/350874207.jpg?k=987985adaff3034f03e090d90d288982e57893391f1f29233a5c90539b6faf49&o=&hp=1",
              width: 600,
              height: 200,
              fit: BoxFit.cover
            ),
            titleSection,
            buttonSection,
            textSection
          ],
        )
      )
    );
  }
}
