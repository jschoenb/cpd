import 'package:flutter/material.dart';
import 'package:timeapp/pages/choose_location.dart';
import 'package:timeapp/pages/home.dart';
import 'package:timeapp/pages/loading.dart';

void main() {
  runApp(MaterialApp(
    //home: Home(),
    routes:{
      "/home" : (context) => const Home(),
      "/": (context) => const Loading(),
      "/location" : (context) => const ChooseLocation()
    },
    //initialRoute: "/home",
  ));
}




