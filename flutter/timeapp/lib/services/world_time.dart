import 'package:http/http.dart';
import 'dart:convert';

import 'package:intl/intl.dart';

class WorldTime {
  String location;
  String ? time;
  String flag; //url to an asset with a flag
  String timeZone;
  bool isDayTime = true;

  WorldTime({required this.location, required this.flag, required this.timeZone});

  Future<void> getTime() async{
    try{
      Uri uri = Uri.https('www.timeapi.io', 'api/Time/current/zone',
          {'timeZone': timeZone});

      // Await the http get response, then decode the json-formatted response.
      Response response = await get(uri);
      if (response.statusCode == 200) {
        Map jsonResponse = jsonDecode(response.body) as Map<String, dynamic>;
        time = jsonResponse['dateTime'];
        DateTime now = DateTime.parse(time!);
        isDayTime = now.hour >6 && now.hour<20 ? true : false;
        time = DateFormat.Hm().format(now);
      } else {
        print('Request failed with status: ${response.statusCode}.');
        time = "Could not get the time of the location";
      }
    } catch (e) {
      print('caught error $e');
      time = "Could not get the time of the location";
    }
  }
}