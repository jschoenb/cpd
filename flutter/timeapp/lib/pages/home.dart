import 'package:flutter/material.dart';
import 'package:timeapp/services/world_time.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  WorldTime? data;

  @override
  Widget build(BuildContext context) {
    data = data ?? ModalRoute.of(context)!.settings.arguments as WorldTime;
    print(data!.location);
    String bgImage = data!.isDayTime ? 'bla.png' : 'night.png';
    print(data!.time);
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage (
            image: AssetImage('assets/$bgImage'),
            fit: BoxFit.cover
          )
        ),
        child: Padding(
          padding: const EdgeInsets.only(top:120.0),
          child: SafeArea(
            child: Column(
              children: [
                TextButton.icon(
                  onPressed: () async{
                    WorldTime result = await Navigator.pushNamed(context, "/location") as WorldTime;
                    print("Result");
                    print(result.time);
                    print(result.location);
                    setState(() {
                      data = result;
                    });
                  },
                  label: const Text("Edit location",
                    style: TextStyle(
                      color: Colors.grey
                    )
                  ),
                  icon: const Icon(Icons.edit_location),
                ),
                const SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(data!.location,
                      style: const TextStyle(
                        fontSize: 30,
                        letterSpacing: 2
                      )
                    )
                  ],
                ),
                const SizedBox(height: 20,),
                Text(data!.time!,
                  style: const TextStyle(fontSize:66)
                )
              ],
            )
          ),
        ),
      ),
    );
  }
}
