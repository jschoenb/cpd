import 'package:flutter/material.dart';

import '../services/world_time.dart';

class ChooseLocation extends StatefulWidget {
  const ChooseLocation({Key? key}) : super(key: key);

  @override
  State<ChooseLocation> createState() => _ChooseLocationState();
}

class _ChooseLocationState extends State<ChooseLocation> {
  List<WorldTime> locations = [
    WorldTime(timeZone: 'Europe/Vienna', location: 'Vienna', flag: 'aut.png'),
    WorldTime(timeZone: 'Europe/London', location: 'London', flag: 'uk.png'),
    WorldTime(timeZone: 'Europe/Berlin', location: 'Berlin', flag: 'germany.png'),
    WorldTime(timeZone: 'Africa/Cairo', location: 'Cairo', flag: 'egypt.png'),
    WorldTime(timeZone: 'Africa/Nairobi', location: 'Nairobi', flag: 'kenya.png'),
    WorldTime(timeZone: 'America/Chicago', location: 'Chicago', flag: 'usa.png'),
    WorldTime(timeZone: 'America/New_York', location: 'New York', flag: 'usa.png'),
    WorldTime(timeZone: 'Asia/Seoul', location: 'Seoul', flag: 'south_korea.png'),
    WorldTime(timeZone: 'Asia/Jakarta', location: 'Jakarta', flag: 'indonesia.png'),
  ];


  @override
  Widget build(BuildContext context) {
    print('in build method');
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: const Text("Choose location"),
        centerTitle: true,
        backgroundColor: Colors.blue[900],
      ),

      body: ListView.builder(
          itemCount: locations.length,
          itemBuilder: (context,index) {
            return Card(
              child: ListTile(
                onTap: () async{
                  WorldTime instance = locations[index];
                  await instance.getTime();
                  if(!mounted)return;
                  Navigator.pop(context,instance);
                },
                title: Text(locations[index].location),
                leading: CircleAvatar(
                  backgroundImage: AssetImage("assets/${locations[index].flag}"),
                ),
              )
            );
          }
      )
    );
  }
}
