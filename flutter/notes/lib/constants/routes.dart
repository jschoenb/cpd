const loginRoute = "/login/";
const registerRoute = "/register/";
const verifyRoute = "/verify/";
const notesRoute = "/notes/";
const homeRoute = "/home/";
const newNoteRoute="/notes/new_note/";
const takePictureRoute = "/notes/picture/";