import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:notes/constants/routes.dart';
import 'package:notes/services/auth/auth_service.dart';
import 'package:notes/views/home_view.dart';
import 'package:notes/views/login_view.dart';
import 'package:notes/views/notes/new_note_view.dart';
import 'package:notes/views/notes/take_picture_view.dart';
import 'package:notes/views/notes_view.dart';
import 'package:notes/views/register_view.dart';
import 'package:notes/views/verify_view.dart';
import 'firebase_options.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await AuthService.firebase().initializeApp();
  final cameras = await availableCameras();
  final firstCamera = cameras.first;
  runApp(MyApp(camera: firstCamera));
}

class MyApp extends StatelessWidget {
  final CameraDescription camera;
  const MyApp({super.key, required this.camera});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const HomeView(),
      routes: {
        loginRoute: (context)=>const LoginView(),
        registerRoute: (context)=>const RegisterView(),
        verifyRoute: (context)=>const VerifyEmailView(),
        notesRoute: (context)=>const NotesView(),
        homeRoute: (context)=>const HomeView(),
        newNoteRoute: (context)=>const NewNoteView(),
        takePictureRoute: (context) => TakePictureView(camera:camera)
      },
    );
  }
}




