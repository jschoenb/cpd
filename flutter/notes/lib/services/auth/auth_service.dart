
import 'package:notes/services/auth/auth_provider.dart';
import 'package:notes/services/auth/auth_user.dart';
import 'package:notes/services/auth/firebase_auth_provider.dart';

class AuthService {
  final AuthProvider provider;

  const AuthService(this.provider);

  factory AuthService.firebase() => AuthService(FirebaseAuthProvider());

  Future<void> initializeApp() => provider.initialize();

  AuthUser? get currentUser => provider.currentUser;

  Future<AuthUser> login ({
    required String email,
    required String password
  }) => provider.login(email: email, password: password);

  Future<AuthUser> createUser ({
    required String email,
    required String password
  }) => provider.create(email: email, password: password);

  Future<void> logout() => provider.logout();
  Future<void> sendEmailVerification() => provider.sendEMailVerification();
}