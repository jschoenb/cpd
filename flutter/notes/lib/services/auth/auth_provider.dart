import 'package:notes/services/auth/auth_user.dart';

abstract class AuthProvider {
  Future<void> initialize();

  AuthUser? get currentUser;

  Future<AuthUser> login({
    required String email,
    required String password
  });

  Future<AuthUser> create({
    required String email,
    required String password
  });

  Future<void> logout();

  Future<void> sendEMailVerification();
}