class UserNotFoundAuthException implements Exception {}
class WrongPasswordAuthException implements Exception {}
class WeakPasswordAuthException implements Exception {}
class EMailAlreadyInUseAuthException implements Exception {}
class InvalidEMailAuthException implements Exception {}
class UserNotLoggedInAuthException implements Exception {}
class GenericAuthException implements Exception {}