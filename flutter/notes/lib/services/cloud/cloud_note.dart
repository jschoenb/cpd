
import 'package:cloud_firestore/cloud_firestore.dart';

const ownerUserFieldName = 'user_id';
const textFieldName = "text";

class CloudNote {
  final String documentId;
  final String ownerUserId;
  final String text;

  const CloudNote({
    required this.documentId,
    required this.ownerUserId,
    required this.text
  });

  CloudNote.fromSnapshot(QueryDocumentSnapshot<Map<String,dynamic>> snapshot):
    documentId = snapshot.id,
    ownerUserId = snapshot.data()[ownerUserFieldName],
    text = snapshot.data()[textFieldName] as String;

  @override
  String toString() => 'Note, ID=$documentId userId=$ownerUserId text=$text';

  @override
  bool operator == (covariant CloudNote other) => documentId == other.documentId;

  @override
  int get hashCode => documentId.hashCode;
}