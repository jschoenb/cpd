
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:notes/services/cloud/cloud_storage_exception.dart';

import 'cloud_note.dart';

class FirebaseCloudStorage {
  //singleton
  static final FirebaseCloudStorage _shared = FirebaseCloudStorage._sharedInstance();
  FirebaseCloudStorage._sharedInstance();

  factory FirebaseCloudStorage() => _shared;

  final notes = FirebaseFirestore.instance.collection('notes');

  //subscribe to all changes on snapshots
  Stream<Iterable<CloudNote>> allNotes({required String ownerUserId}) =>
    notes.snapshots().map((event)=>event.docs.map((doc)=>CloudNote.fromSnapshot(doc)).
    where((note) => note.ownerUserId == ownerUserId));

  Future<CloudNote> createNewNote({required String ownerUserId}) async {
    final document = await notes.add({
        ownerUserFieldName : ownerUserId,
        textFieldName : ''
    });
    final fetchedNote = await document.get();
    return CloudNote(documentId: fetchedNote.id, ownerUserId: ownerUserId, text: '');
  }

  Future<void> updateNote({required String documentId, required String text}) async {
    try {
      await notes.doc(documentId).update({textFieldName : text});
    } catch (e){
      throw CouldNotUpdateNoteException();
    }
  }

  Future<void> deleteNote({required String documentId}) async {
    try {
      await notes.doc(documentId).delete();
    } catch (e){
      throw CouldNotDeleteNoteException();
    }
  }

  Future<Iterable<CloudNote>> getNotes({required String ownerUserId}) async {
    try {
      return await notes.where(
        ownerUserFieldName,
        isEqualTo: ownerUserId
      ).get().then( (value) => value.docs.map((doc) {return CloudNote.fromSnapshot(doc);}));
    } catch (e) {
      throw CouldNotGetAllNotesException();
    }
  }
}