import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:notes/constants/routes.dart';
import 'package:notes/services/auth/auth_exceptions.dart';
import 'package:notes/services/auth/auth_service.dart';
import 'package:toast/toast.dart';

class RegisterView extends StatefulWidget {
  const RegisterView({Key? key}) : super(key: key);

  @override
  State<RegisterView> createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView> {
  late final TextEditingController _email;
  late final TextEditingController _password;

  @override
  void initState() {
    _email = TextEditingController();
    _password = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _email.dispose();
    _password.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ToastContext().init(context);
    return Scaffold(
        appBar: AppBar(
            title: const Text("Register new user")
        ),
        body: Column(
          children: [
            TextField(
              controller: _email,
              enableSuggestions: false,
              autocorrect: false,
              keyboardType: TextInputType.emailAddress,
              decoration: const InputDecoration(
                  labelText: "E-Mail",
                  hintText: "Enter your email here"
              ),
            ),
            TextField(
              controller: _password,
              enableSuggestions: false,
              autocorrect: false,
              obscureText: true,
              decoration: const InputDecoration(
                  labelText: "Password",
                  hintText: "Enter your password here"
              ),
            ),
            TextButton(
                onPressed: () async {
                  final email = _email.text;
                  final password = _password.text;
                  try {
                    AuthService.firebase().createUser(
                        email: email, password: password);
                    AuthService.firebase().sendEmailVerification();
                    final user = AuthService.firebase().currentUser;
                    if(!mounted) return;
                    Navigator.of(context).pushNamed(loginRoute);
                  } on EMailAlreadyInUseAuthException catch(e){
                      Toast.show("E-Mail already in use!");
                  } on InvalidEMailAuthException catch(e){
                    Toast.show("You entered an invalid email address!");
                  } on WeakPasswordAuthException catch(e){
                    Toast.show("Your password is too weak!");
                  } on GenericAuthException catch(e){
                    Toast.show("Sorry, could not register new user!");
                  }
                },
                child: const Text("Register")
            ),
            TextButton(
                onPressed: () {
                  Navigator.of(context).pushNamedAndRemoveUntil(loginRoute, (route) => false);
                },
                child: const Text("Already registered? Login here")            )
          ],
        )
    );
  }
}
