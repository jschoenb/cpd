import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:notes/constants/routes.dart';
import 'package:notes/services/auth/auth_service.dart';
import 'package:notes/services/cloud/firebase_cloud_storage.dart';

import '../services/cloud/cloud_note.dart';

class NotesView extends StatefulWidget {
  const NotesView({Key? key}) : super(key: key);

  @override
  State<NotesView> createState() => _NotesViewState();
}

enum MenuAction {logout}

class _NotesViewState extends State<NotesView> {

  String get userId => AuthService.firebase().currentUser!.id;
  late final FirebaseCloudStorage _service;

  void initState(){
    _service = FirebaseCloudStorage();
    super.initState();
  }

  Future<bool> showLogoutDialog(BuildContext context){
    return showDialog<bool> (context: context, builder: (context){
      return AlertDialog(
        title: const Text("Sign out"),
        content: const Text("Are you sure you want to sign out"),
        actions: [
          TextButton(
              onPressed: (){
                Navigator.of(context).pop(false);
              },
              child: const Text("Cancel")
          ),
          TextButton(
              onPressed: (){
                Navigator.of(context).pop(true);
              },
              child: const Text("Log out")
          )
        ],
      );
    }).then((value)=>value??false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Your notes"),
        actions: [
          IconButton(
            onPressed: (){
              Navigator.of(context).pushNamed(newNoteRoute);
            },
            icon: const Icon(Icons.add)
          ),
          PopupMenuButton(
            onSelected: (value) async {
              print(value.toString());
              switch(value){
                case MenuAction.logout:
                  final shouldLogout = await showLogoutDialog(context);
                  if(shouldLogout){
                    await AuthService.firebase().logout();
                    if(!mounted) return;
                    Navigator.of(context).pushNamedAndRemoveUntil(loginRoute, (route) => false);
                  }
                  break;
              }
            },
              itemBuilder: (context) {
                return const [
                  PopupMenuItem<MenuAction>(
                    value: MenuAction.logout,
                      child: Text("Logout")
                  )
                ];
              },)
        ],
      ),
      body: StreamBuilder(
        stream: _service.allNotes(ownerUserId: userId),
        builder: (context, snapshot) {
          switch (snapshot.connectionState){
            case ConnectionState.waiting:
            case ConnectionState.active:
              if(snapshot.hasData){
                final allNotes = snapshot.data as Iterable<CloudNote>;
                print(allNotes);
                return ListView.builder(
                  itemCount: allNotes.length,
                  itemBuilder: (context,index){
                    final note = allNotes.elementAt(index);
                    return ListTile (
                      title: Text(note.text,maxLines: 1,softWrap: true,overflow: TextOverflow.ellipsis),
                      onTap: ()async{
                        Navigator.of(context).pushNamed(newNoteRoute,arguments: note);
                      },
                      trailing: IconButton(
                        onPressed: () async {
                          await _service.deleteNote(documentId: note.documentId);
                        },
                        icon: const Icon(Icons.delete),
                      ),
                    );
                  }
                );
              } else {
                return const CircularProgressIndicator();
              }

            default: return const CircularProgressIndicator();
          }
        },
      )
    );
  }
}
