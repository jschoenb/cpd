import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:notes/constants/routes.dart';
import 'package:notes/services/auth/auth_exceptions.dart';
import 'package:toast/toast.dart';

import '../services/auth/auth_service.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  late final TextEditingController _email;
  late final TextEditingController _password;

  @override
  void initState() {
    _email = TextEditingController();
    _password = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _email.dispose();
    _password.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ToastContext().init(context);
    return Scaffold(
        appBar: AppBar(
            title: const Text("Login")
        ),
        body: Column(
          children: [
            TextField(
              controller: _email,
              enableSuggestions: false,
              autocorrect: false,
              keyboardType: TextInputType.emailAddress,
              decoration: const InputDecoration(
                  labelText: "E-Mail",
                  hintText: "Enter your email here"
              ),
            ),
            TextField(
              controller: _password,
              enableSuggestions: false,
              autocorrect: false,
              obscureText: true,
              decoration: const InputDecoration(
                  labelText: "Password",
                  hintText: "Enter your password here"
              ),
            ),
            TextButton(
                onPressed: () async {
                  final email = _email.text;
                  final password = _password.text;
                  try {
                    final userCredentials = await AuthService.firebase().login(
                        email: email, password: password);
                    print(userCredentials);
                    Toast.show("Successfully logged in");
                    if(!mounted) return;
                    Navigator.of(context).pushNamedAndRemoveUntil(homeRoute, (route) => false);
                  } on UserNotFoundAuthException catch(e){
                      Toast.show("User could not be found");
                  } on WrongPasswordAuthException catch(e){
                    Toast.show("Wrong credentials!");
                  } on GenericAuthException catch(e){
                    Toast.show("Authentication error");
                  }
                },
                child: const Text("Login")
            ),
            TextButton(
                onPressed: (){
                  Navigator.of(context).pushNamedAndRemoveUntil(
                     registerRoute, (route) => false);
                },
                child: const Text("Not registered yet? Register here!")
            )
          ],
        )
    );
  }
}
