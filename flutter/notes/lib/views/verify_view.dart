import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:notes/constants/routes.dart';
import 'package:notes/services/auth/auth_service.dart';

class VerifyEmailView extends StatefulWidget {
  const VerifyEmailView({Key? key}) : super(key: key);

  @override
  State<VerifyEmailView> createState() => _VerifyEmailViewState();
}

class _VerifyEmailViewState extends State<VerifyEmailView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Verify Email")
      ),
      body: Column (
        children: [
          const Text("We have sent an email verification - pleas check your email"),
          const Text("If you have not received an email yet, press the button blow"),
          TextButton(
              onPressed: ()async{
               await AuthService.firebase().sendEmailVerification();
                if(!mounted) return;
                Navigator.of(context).pushNamedAndRemoveUntil(loginRoute,
                        (route) => false);
              },
              child: const Text("Send email verification")
          ),
          TextButton(
              onPressed: ()async{
                await AuthService.firebase().logout();
                if(!mounted) return;
                Navigator.of(context).pushNamedAndRemoveUntil(loginRoute,
                        (route) => false);
              },
              child: const Text("Back to login")
          )
        ],
      ),
    );
  }
}
