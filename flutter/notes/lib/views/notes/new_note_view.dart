import 'package:flutter/material.dart';
import 'package:notes/services/auth/auth_service.dart';
import 'package:notes/services/cloud/cloud_note.dart';
import 'package:notes/services/cloud/firebase_cloud_storage.dart';
import 'package:notes/utilities/get_arguments.dart';

class NewNoteView extends StatefulWidget {
  const NewNoteView({Key? key}) : super(key: key);

  @override
  State<NewNoteView> createState() => _NewNoteViewState();
}

class _NewNoteViewState extends State<NewNoteView> {
  CloudNote? _note;
  late final FirebaseCloudStorage _notesService;
  late final TextEditingController _textController;

  void _textControllerListener() async {
    final note = _note;
    if(note == null){
      return;
    }
    await _notesService.updateNote(documentId: note.documentId, text: _textController.text);
  }

  void _setupTextControllerListener() {
    _textController.removeListener(_textControllerListener);
    _textController.addListener(_textControllerListener);
  }

  void _deleteNoteIfTextIsEmpty() {
    final note = _note;
    if(_textController.text.isEmpty && note != null){
      _notesService.deleteNote(documentId: note.documentId);
    }
  }

  void _saveNoteIfTextNotEmpty() async {
    final note = _note;
    if(_textController.text.isNotEmpty && note != null) {
      await _notesService.updateNote(documentId: note.documentId, text: _textController.text);
    }
  }

  @override
  void initState() {
    _notesService = FirebaseCloudStorage();
    _textController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _deleteNoteIfTextIsEmpty();
    _saveNoteIfTextNotEmpty();
    _textController.dispose();
    super.dispose();
  }

  Future<CloudNote> createOrGetExistingNote(BuildContext context) async {
    final existingNote = context.getArgument<CloudNote>();
    print(existingNote);
    if(existingNote != null){
      _textController.text = existingNote.text;
      _note = existingNote;
      return existingNote;
    }
    final currentUser = AuthService.firebase().currentUser!;
    final newNote = await _notesService.createNewNote(ownerUserId: currentUser.id);
    _note = newNote;
    return newNote;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        title: const Text("New Note")
      ),
      body: FutureBuilder(
        future: createOrGetExistingNote(context),
        builder : (context,snapshot){
          switch (snapshot.connectionState){
            case ConnectionState.done:
              _note = snapshot.data as CloudNote;
              _setupTextControllerListener();
              return TextField(
                controller: _textController,
                keyboardType: TextInputType.multiline,
                maxLines: null,
                decoration: const InputDecoration(
                  hintText: "Start typing your new note..."
                ),
              );
            default:
                return const CircularProgressIndicator();
          }
        }
      )
    );
  }
}
