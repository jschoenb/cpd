import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {

  late final TextEditingController _email;
  late final TextEditingController _password;

  @override
  void initState() {
    _email = TextEditingController();
    _password = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _email.dispose();
    _password.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    ToastContext().init(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Login"),
      ),
      body: Column(
        children: [
          TextField(
            controller: _email,
            enableSuggestions: false,
            autocorrect: false,
            keyboardType: TextInputType.emailAddress,
            decoration: const InputDecoration(
                labelText: "E-Mail",
                hintText: 'Enter your email here'
            ),
          ),
          TextField(
            controller: _password,
            obscureText: true,
            enableSuggestions: false,
            autocorrect: false,
            decoration: const InputDecoration(
                hintText: 'Enter your password here'
            ),
          ),
          TextButton(
            onPressed: () async {
              print("Button pressed");
              final email = _email.text;
              final password = _password.text;
              try{
                final userCredential = await FirebaseAuth.instance.signInWithEmailAndPassword(email: email, password: password);
                print(userCredential);
              }on FirebaseAuthException catch (e){
                print(e);
                if(e.code == "email-already-in-use"){
                  Toast.show("E-mail already in use!");
                }
              }
            },
            child: const Text("Login"),
          ),
        ],
      ),
    );

  }
}