import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:notestest/lib/verify_view.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Home"),
        ),
        body: showWidget(context)
    );
  }

  showWidget(BuildContext context) {
    final user = FirebaseAuth.instance.currentUser;
    if(user != null){
      if(user?.emailVerified ?? false){
        print("You are a verified user");
        SchedulerBinding.instance.addPostFrameCallback((_) {
          Navigator.of(context).pushNamedAndRemoveUntil("/notes/",
                  (route) => false);
        });
      } else {
        print("Your email must be verified");
        SchedulerBinding.instance.addPostFrameCallback((_) {
          Navigator.of(context).pushNamedAndRemoveUntil("/verify/",
                  (route) => false);
        });
      }
    } else {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Navigator.of(context).pushNamedAndRemoveUntil("/login/",
                (route) => false);
      });
    }
  }
}
